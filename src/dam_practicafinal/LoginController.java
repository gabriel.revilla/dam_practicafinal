package dam_practicafinal;

import java.io.File;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.ResourceBundle;

import dam_practicafinal.entidades.Jugador;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class LoginController implements Initializable{
	private LoginController self;
	private Main main;
	
	@FXML
	public Button btnSignup;
	@FXML
	public TextField inLoginUsername;
	@FXML
	public TextField inLoginPassword;
	@FXML
	public TextField inSignupUsername;
	@FXML 
	public TextField inSignupPassword;
	@FXML
	public ChoiceBox inSignupSexo;
	@FXML
	public DatePicker inSignupFechaNacim;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		 try {
			 
		 } catch (Exception ex) {
			 System.err.print("error " + ex.getMessage());
			 ex.printStackTrace();
		 }
	}
	
	public void start(Main main) {
		this.main = main;
		
		ObservableList<String> itemsSexo = FXCollections.observableArrayList("Hombre", "Mujer", "Otro");
		this.inSignupSexo.setItems(itemsSexo);
	}
	
	@FXML
	public void registerKey(KeyEvent event) {
		if(event.getCode().getCode() == 10) {
			clickSignup();
		}
	}
	
	@FXML
	public void loginKey(KeyEvent event) {
		if(event.getCode().getCode() == 10) {
			clickLogin();
		}
	}
	
	@FXML
	public void clickLogin() {
		System.out.println("LOGIN!");
		Jugador jugador = new Jugador();
		if(jugador.login(inLoginUsername.getText(), inLoginPassword.getText())) {
			main.showMain(jugador);
		}else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pokemon final");
			alert.setHeaderText("Login response");
			alert.setContentText("Usuario o contraseña incorrectos!");

			alert.showAndWait();
		}
	}
	
	@FXML
	public void clickSignup() {
		System.out.println("SIGN UP!");
		Jugador jugador = new Jugador();
		
		if(jugador.register(inSignupUsername.getText(), 
				inSignupPassword.getText(), 
				inSignupSexo.getSelectionModel().getSelectedItem().toString(), 
				Date.valueOf(inSignupFechaNacim.getValue()))) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pokemon final");
			alert.setHeaderText("Login response");
			alert.setContentText("Has creado tu cuenta!");

			alert.showAndWait();
		}else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pokemon final");
			alert.setHeaderText("Login response");
			alert.setContentText("No se ha creado la cuenta!");

			alert.showAndWait();
		}
	}
}
