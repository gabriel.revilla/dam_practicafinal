package dam_practicafinal;

import java.io.File;

import dam_practicafinal.entidades.Jugador;
import dam_practicafinal.tools.DbHwnd;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class Main extends Application{
	private DbHwnd dbHwnd;
	private LoginController loginController;
	private MainController mainController;
	private Stage stage;
	private Scene sceneLogin;
	private Scene sceneMain;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader fxmlMain = new FXMLLoader(getClass().getResource("main.fxml"));
			FXMLLoader fxmlLogin = new FXMLLoader(getClass().getResource("login.fxml"));
			
			this.stage = primaryStage;
			
			Parent rootMain = fxmlMain.load();
			Parent rootLogin = fxmlLogin.load();
			
			sceneLogin = new Scene(rootLogin);
			sceneLogin.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
			
			sceneMain = new Scene(rootMain);
			sceneMain.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
			
			mainController = fxmlMain.getController();
			loginController = fxmlLogin.getController();
			
			primaryStage.setScene(sceneLogin);
			primaryStage.setTitle("Pokemon practica final");
			primaryStage.setResizable(false);
			primaryStage.show();
			
			dbHwnd = new DbHwnd(new File("config.json"));
			
			loginController.start(this);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void showMain(Jugador idPlayer) {
		stage.setScene(sceneMain);
		stage.setTitle("Pokemon practica final - Main");
		stage.setResizable(false);
		
		mainController.setJugador(idPlayer);
		
		stage.show();
	}

	public static void main(String[] args) {
		launch();
	}
}
