package dam_practicafinal;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import dam_practicafinal.entidades.Jugador;
import dam_practicafinal.entidades.Objeto;
import dam_practicafinal.entidades.Pokedex;
import dam_practicafinal.entidades.Pokemon;
import dam_practicafinal.entidades.TiendaStock;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.text.Text;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.util.Callback;

public class MainController implements Initializable{
	private MainController self;
	@FXML
	private ImageView imgCapturaPokemon;
	@FXML
	private TextArea txtPokemonStats;
	@FXML
	private ChoiceBox<String> inPokeballSelect;
	@FXML
	private ImageView imgPokeballSelect;
	@FXML
	private Text txtPokeballsCantidad;
	@FXML
	private ChoiceBox<String> inTiendaSelect;
	@FXML
	private TextField inTiendaCantidad;
	@FXML
	private Text txtPrecioTienda;
	@FXML
	private Text txtTienesTienda;
	@FXML
	private Text txtTiendaPokecuartos;
	@FXML
	private ImageView imgObjetoTienda;
	
	/////////////Tabla pokemons////////////////
	@FXML
	private TableView<PokemonsTable> tablePokemons;
	@FXML
	private TableColumn<PokemonsTable, String> columnPokemonEspecie;
	@FXML
	private TableColumn<PokemonsTable, String> columnPokemonMote;
	@FXML
	private TableColumn<PokemonsTable, String> columnPokemonNivel;
	@FXML
	private TableColumn<PokemonsTable, String> columnPokemonVida;
	@FXML
	private TableColumn<PokemonsTable, String> columnPokemonVidaActual;
	@FXML
	private TableColumn<PokemonsTable, String> columnPokemonAtaque;
	@FXML
	private TableColumn<PokemonsTable, String> columnPokemonDefensa;
	
	/////////////tablaObjetos//////////////
	@FXML
	private TableView<ObjetosTable> tableObjetos;
	@FXML
	private TableColumn<ObjetosTable, String> columnObjetosTipo;
	@FXML
	private TableColumn<ObjetosTable, String> columnObjetosNombre;
	@FXML
	private TableColumn<ObjetosTable, String> columnObjetosCantidad;
	
	private Objeto pokeballSelected;
	private Pokemon pokemonCapturaActual;
	private int tiendaObjetoSelected;
	private Jugador jugador;
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		 try {
			 
		 } catch (Exception ex) {
			 System.err.print("error " + ex.getMessage());
			 ex.printStackTrace();
		 }
	}
	
	@FXML void clickCapturar() {
		int rnd = (int) (Math.random() * 1000 + 1);
		
		if(pokemonCapturaActual != null) {
			if(pokeballSelected.usarObjeto(1)) {
				if(rnd <= 600) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Pokemon final");
					alert.setHeaderText("Captura pokemon");
					alert.setContentText("Has capturado un " + pokemonCapturaActual.getEspecie() + " con nivel " + pokemonCapturaActual.getNivel());
	
					alert.showAndWait();
					
					TextInputDialog dialog = new TextInputDialog("walter");
					dialog.setTitle("Pokemon final");
					dialog.setHeaderText("Captura pokemon");
					dialog.setContentText("Quieres ponerle un mote?");
	
					// Traditional way to get the response value.
					Optional<String> result = dialog.showAndWait();
					if (result.isPresent()){
						pokemonCapturaActual.setMote(result.get());
					}
					
					this.jugador.getMochila().addPokemon(pokemonCapturaActual);
					
					this.jugador.getMochila().addPkc(150);
					
					pokemonCapturaActual = null;
					this.imgCapturaPokemon.setImage(null);
					this.txtPokemonStats.setText("");
				}else {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Pokemon final");
					alert.setHeaderText("Captura pokemon");
					alert.setContentText("Pokemon no capturado, se está resistiendo!");
	
					alert.showAndWait();
				}
			}else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Pokemon final");
				alert.setHeaderText("Captura pokemon");
				alert.setContentText("No te quedan pokeballs!");
	
				alert.showAndWait();
			}
		}
		
		if(this.jugador.getMochila().guardarDb()) {
			System.out.println("GUARDADO EN BD!");
		}
		
		this.actualizar();
	}
	
	private void buscarPokemon() {
		System.out.println("BUSCAR POKEMON!");
		Pokedex pokedex = new Pokedex();
		
		JsonArray pokemonArray = pokedex.getAllPokedex();
		int randomPokemon = (int) (Math.random() * pokemonArray.size() + 1);
		int randomNivel = (int) (Math.random() * 100 + 1);
		
		JsonObject pokemonFinal = pokemonArray.get(randomPokemon).getAsJsonObject();
		
		Pokemon nuevoPokemon = new Pokemon(pokemonFinal.get("id").getAsInt(), randomNivel);
		
		String pokedexNum = "000" + String.valueOf(nuevoPokemon.getIdPokedex());
		pokedexNum = pokedexNum.substring(pokedexNum.length() - 3, pokedexNum.length());
		
		imgCapturaPokemon.setImage(new Image("/thumbnails/" + pokedexNum + ".png"));
		
		String tipos = "";
		
		for(int i=0;i<nuevoPokemon.getTipos().size();i++) {
			if(i == nuevoPokemon.getTipos().size()) {
				tipos += nuevoPokemon.getTipos().get(i) + "/";
			}else {
				tipos += nuevoPokemon.getTipos().get(i) + " ";
			}
		}
		
		this.pokemonCapturaActual = nuevoPokemon;
		
		txtPokemonStats.setText("Ha aparecido un " + nuevoPokemon.getEspecie() + " salvaje\nNivel: " + nuevoPokemon.getNivel() + "\nTipo: " + tipos);
	}
	
	@FXML
	public void clickBuscarPokemon() {
		this.buscarPokemon();
	}
	
	@FXML
	public void clickCombate() {
		System.out.println("COMBATE!");
		
		if(this.jugador.getMochila().getPokemones().size() > 0) {
			
		}else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Pokemon final");
			alert.setHeaderText("Sin pokemones");
			alert.setContentText("No tienes ningún pokemon para combatir");

			alert.showAndWait();
		}
	}
	
	@FXML
	public void clickComprarTienda() {
		int id = inTiendaSelect.getSelectionModel().getSelectedIndex() + 1;
		String cant = inTiendaCantidad.getText();
		if(!cant.isEmpty()) {
			try {
				int cantidad = Integer.parseInt(inTiendaCantidad.getText());
				
				Objeto nuevoObjeto = new Objeto(id, cantidad);
				
				TiendaStock tiendaStock = new TiendaStock();
				
				JsonArray stockArr  = tiendaStock.getAllObjetos();
				JsonObject stockObj = null;
				
				for(int i=0;i<stockArr.size();i++) {
					if(stockArr.get(i).getAsJsonObject().get("id").getAsInt() == id) {
						stockObj = stockArr.get(i).getAsJsonObject();
					}
				}
				
				if(stockObj != null) {
					if(this.jugador.getMochila().descontarPkc(stockObj.get("coste").getAsInt()*cantidad)) {
						this.jugador.getMochila().addObjetos(nuevoObjeto);
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Pokemon final");
						alert.setHeaderText("Tienda");
						alert.setContentText("Has comprado " + cantidad + " " + stockObj.get("nombre").getAsString());

						alert.showAndWait();
					}else {
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("Pokemon final");
						alert.setHeaderText("Tienda");
						alert.setContentText("No tienes suficientes pokecuartos para comprar eso!");

						alert.showAndWait();
					}
				}
			}catch(NumberFormatException ex){
				
				this.inTiendaCantidad.setText("");
			}
			
			
			
			this.jugador.getMochila().guardarDb();
			this.updatePokeballs();
			this.updateTiendaObjetos();
			this.txtTiendaPokecuartos.setText(this.jugador.getMochila().getPokecuartos() + " PKC");
		}
		
	}
	
	private void updateTiendaInfo() {
		int id = inTiendaSelect.getSelectionModel().getSelectedIndex() + 1;
	
		TiendaStock tiendaStock = new TiendaStock();
		
		JsonArray stockArr  = tiendaStock.getAllObjetos();
		JsonObject objObject = null;

		for(int i=0;i<stockArr.size();i++) {
			JsonObject obj = stockArr.get(i).getAsJsonObject();
			
			if(obj.get("id").getAsInt() == id) {
				objObject = obj; 
			}
		}
		
		if(objObject != null) {
			txtPrecioTienda.setText(String.valueOf(objObject.get("coste")));
			imgObjetoTienda.setImage(new Image("/Objetos/" + objObject.get("id").getAsInt() + ".png"));
			
			int tienes = 0;
			
			for(int i=0;i<jugador.getMochila().getObjetos().size();i++) {
				if(jugador.getMochila().getObjetos().get(i).getObjetoId() == objObject.get("id").getAsInt()) {
					tienes = jugador.getMochila().getObjetos().get(i).getCantidad();
				}
			}
			
			txtTienesTienda.setText(String.valueOf(tienes));
		}
	}
	
	private void updateTiendaObjetos() {
		ObservableList<String> objetos = FXCollections.observableArrayList();
		
		TiendaStock tiendaStock = new TiendaStock();
		
		JsonArray stockArr  = tiendaStock.getAllObjetos();
		

		for(int i=0;i<stockArr.size();i++) {
			JsonObject obj = stockArr.get(i).getAsJsonObject();
			
			objetos.add(obj.get("nombre").getAsString());
		}
		
		inTiendaSelect.setItems(objetos);
		
		inTiendaSelect.getSelectionModel().select(0);
		updateTiendaInfo();
	}
	
	private void updatePokeballs() {
		ObservableList<String> pokeballs = FXCollections.observableArrayList();
		
		for(int i=0;i<this.jugador.getMochila().getObjetos().size();i++) {
			Objeto obj = this.jugador.getMochila().getObjetos().get(i);
			
			if(obj.getTipo().equals("pokeball")) {
				pokeballs.add(obj.getNombre());
			}
		}
		
		inPokeballSelect.setItems(pokeballs);
		
		inPokeballSelect.getSelectionModel().select(0);
		updatePokeballsText();
	}
	
	private void updatePokeballsText() {
		String objIndex = inPokeballSelect.getSelectionModel().getSelectedItem();
		Objeto pokeballSel = null;
		
		for(int i=0;i<jugador.getMochila().getObjetos().size();i++) {
			Objeto obj = jugador.getMochila().getObjetos().get(i);
			
			if(obj.getNombre() == objIndex) {
				pokeballSel = obj;
			}
		}
		
		if(pokeballSel != null) {
			pokeballSelected = pokeballSel;
			txtPokeballsCantidad.setText("Quedan " + String.valueOf(pokeballSelected.getCantidad()) + " " + pokeballSelected.getNombre());
		}else {
			updatePokeballs();
		}
	}
	
	
	///////////////////MOCHILA////////////////////////
	
	private void updateMochilaTablas() {
		ObservableList<PokemonsTable> dataPokemons = FXCollections.observableArrayList();
		
		for(int i=0;i<this.jugador.getMochila().getPokemones().size();i++) {
			Pokemon pokemon = this.jugador.getMochila().getPokemones().get(i);
			 
			PokemonsTable row = new PokemonsTable(
					pokemon.getId(),
					pokemon.getEspecie(),
					pokemon.getMote(),
					String.valueOf(pokemon.getNivel()), 
					String.valueOf(pokemon.getPs()),
					String.valueOf(pokemon.getPsActual()),
					String.valueOf(pokemon.getAtaque()), 
					String.valueOf(pokemon.getDefensa()));
			dataPokemons.add(row);
		}
		
		ObservableList<ObjetosTable> dataObjetos = FXCollections.observableArrayList();
		
		for(int i=0;i<this.jugador.getMochila().getObjetos().size();i++) {
			Objeto objeto = this.jugador.getMochila().getObjetos().get(i);
			 
			ObjetosTable row = new ObjetosTable(
					objeto.getTipo(),
					objeto.getNombre(),
					objeto.getCantidad());
			
			dataObjetos.add(row);
		}
		
		this.tablePokemons.setItems(dataPokemons);
		this.tableObjetos.setItems(dataObjetos);
	}
	
	///////////////////END MOCHILA////////////////////
	
	
	private void actualizar() {
		this.jugador.getMochila().guardarDb();
		this.updatePokeballs();
		this.updateTiendaObjetos();
		this.updateMochilaTablas();
		this.txtTiendaPokecuartos.setText(this.jugador.getMochila().getPokecuartos() + " PKC");
	}
	
	@FXML
	public void initCapturar() {
		if(this.jugador != null) {
			this.actualizar();
		}
	}
	
	@FXML 
	public void initPokemones() {
		if(this.jugador != null) {
			this.actualizar();
		}
	}
	
	@FXML
	public void initObjetos() {
		if(this.jugador != null) {
			this.actualizar();
		}
	}
	
	@FXML
	public void initTienda() {
		if(this.jugador != null) {
			this.actualizar();
		}
	}
	
	public void setJugador(Jugador _jugador) {
		this.jugador = _jugador;
		this.pokemonCapturaActual = null;
		
		columnPokemonEspecie.setCellValueFactory(new PropertyValueFactory<PokemonsTable, String>("especie"));
		columnPokemonMote.setCellValueFactory(new PropertyValueFactory<PokemonsTable, String>("mote"));
		columnPokemonNivel.setCellValueFactory(new PropertyValueFactory<PokemonsTable, String>("nivel"));
		columnPokemonAtaque.setCellValueFactory(new PropertyValueFactory<PokemonsTable, String>("ataque"));
		columnPokemonDefensa.setCellValueFactory(new PropertyValueFactory<PokemonsTable, String>("defensa"));
		columnPokemonVida.setCellValueFactory(new PropertyValueFactory<PokemonsTable, String>("vida"));
		columnPokemonVidaActual.setCellValueFactory(new PropertyValueFactory<PokemonsTable, String>("vidaActual"));
		tablePokemons.setRowFactory(tv -> {
		    TableRow<PokemonsTable> row = new TableRow<>();
		    row.setOnMouseClicked(event -> {
		        if (! row.isEmpty() && event.getButton()==MouseButton.PRIMARY 
		             && event.getClickCount() == 2) {

		            PokemonsTable clickedRow = row.getItem();
		            
		            Alert alert = new Alert(AlertType.CONFIRMATION);
		            alert.setTitle("Liberar pokemon");
		            alert.setHeaderText("Liberar pokemon");
		            alert.setContentText("Quieres liberar tu pokemon llamado " + clickedRow.getMote() + "?");

		            Optional<ButtonType> result = alert.showAndWait();
		            if (result.get() == ButtonType.OK){
		            	jugador.getMochila().removePokemon(clickedRow.getId());
		            	
		            	actualizar();
		            }
		        }
		    });
		    
		    return row ;
		});
		
		columnObjetosTipo.setCellValueFactory(new PropertyValueFactory<ObjetosTable, String>("tipo"));
		columnObjetosNombre.setCellValueFactory(new PropertyValueFactory<ObjetosTable, String>("nombre"));
		columnObjetosCantidad.setCellValueFactory(new PropertyValueFactory<ObjetosTable, String>("cantidad"));
		
		this.updatePokeballs();
		this.updateTiendaObjetos();
		this.txtTiendaPokecuartos.setText(this.jugador.getMochila().getPokecuartos() + " PKC");
		updateMochilaTablas();
		
		System.out.println("ID PLAYER: " + this.jugador.getId());
		
		inPokeballSelect.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				updatePokeballsText();
			}
			
		});
		
		inTiendaSelect.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				updateTiendaInfo();
			}
			
		});
	}
	
	public class PokemonsTable{
		private SimpleStringProperty especie;
		private SimpleStringProperty mote;
		private SimpleStringProperty nivel;
		private SimpleStringProperty ataque;
		private SimpleStringProperty defensa;
		private SimpleStringProperty vida;
		private SimpleStringProperty vidaActual;
		private int id;
		
		public PokemonsTable(int id, String especie, String mote, String nivel, String vida, String vidaActual, String ataque, String defensa){
			this.especie = new SimpleStringProperty(especie);
			this.mote = new SimpleStringProperty(mote);
			this.nivel = new SimpleStringProperty(nivel);
			this.ataque = new SimpleStringProperty(ataque);
			this.defensa = new SimpleStringProperty(defensa);
			this.vida = new SimpleStringProperty(vida);
			this.vidaActual = new SimpleStringProperty(vidaActual);
		}
		
		public int getId() {
			return this.id;
		}
		
		public String getEspecie() {
			return especie.get();
		}
		
		public String getMote() {
			return mote.get();
		}
		
		public String getNivel() {
			return nivel.get();
		}
		
		public String getAtaque() {
			return ataque.get();
		}
		
		public String getDefensa() {
			return defensa.get();
		}
		
		public String getVida() {
			return vida.get();
		}
		
		public String getVidaActual() {
			return vidaActual.get();
		}
	}
	
	public class ObjetosTable{
		private SimpleStringProperty tipo;
		private SimpleStringProperty nombre;
		private SimpleStringProperty cantidad;
		

		public ObjetosTable(String tipo, String nombre, int cantidad){
			this.tipo = new SimpleStringProperty(tipo);
			this.nombre = new SimpleStringProperty(nombre);
			this.cantidad = new SimpleStringProperty(String.valueOf(cantidad));
		}
		
		public String getTipo() {
			return tipo.get();
		}
		
		public String getNombre() {
			return nombre.get();
		}
		
		public String getCantidad() {
			return cantidad.get();
		}
		
		
	}
}
