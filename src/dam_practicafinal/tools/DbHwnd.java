package dam_practicafinal.tools;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DbHwnd {
	private Connection conn;
	
	public DbHwnd(String host, String username, String passwd, String database) {
		try {
			conn = DriverManager.getConnection("jdbc:mariadb://" + host 
					+ ":3306/" + database
					+ "?user=" + username
					+ "&password=" + passwd);
		} catch (SQLException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de conexión");
			alert.setHeaderText("Error de BBDD");
			alert.setContentText("Error al conectar con la BBDD");

			alert.showAndWait();
		}
	}
	
	public DbHwnd(File configFile) {
		JsonParser parser = new JsonParser();
        JsonObject configObj;
        
		try {
			configObj = parser.parse(new FileReader(configFile)).getAsJsonObject();
			
			JsonObject dbObj = configObj.get("dbConfig").getAsJsonObject();
	        
			try {
				conn = DriverManager.getConnection("jdbc:mariadb://" + dbObj.get("host").getAsString() 
						+ ":3306/" + dbObj.get("database").getAsString()
						+ "?user=" + dbObj.get("user").getAsString() 
						+ "&password=" + dbObj.get("password").getAsString());
				
			} catch (SQLException e) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Error de conexión");
				alert.setHeaderText("Error de BBDD");
				alert.setContentText("Error al conectar con la BBDD");

				alert.showAndWait();
			}
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Error de conexión");
			alert.setHeaderText("Error de BBDD");
			alert.setContentText("Error al conectar con la BBDD");

			alert.showAndWait();
		}
	}
	
	public ResultSet query(PreparedStatement prepStmt) {
		ResultSet rs = null;
		
		try {
			rs = prepStmt.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}
        
     public boolean insert(PreparedStatement prepStmt){
            try {
                prepStmt.executeQuery();
                
                return true;
            } catch (SQLException ex) {
                Logger.getLogger(DbHwnd.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
     }
        
     public Connection getConnection(){
         return conn;
     }
     
}
