package dam_practicafinal.entidades;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class Pokemon {
	private int idPokedex;
	private int id;

	private int ps;
	private int psActual;
	private int ataque;
	private int defensa;
	private int nivel;
	private int velocidad;
	private int ataqueEspecial;
	private int defensaEspecial;
	private ArrayList<String> tipos;

	private int psBase;
	private int ataqueBase;
	private int defensaBase;
	private int velocidadBase;
	private int ataqueEspecialBase;
	private int defensaEspecialBase;

	private String especie;
	private String mote;
	
	public Pokemon(int pokedexId, int nivel) {
		this.tipos = new ArrayList<String>();
		
		crearPokemon(pokedexId, nivel, null);
		this.id = -1;
	}

	public Pokemon(int pokedexId, int nivel, String mote, int idDb) {
		this.tipos = new ArrayList<String>();
		
		crearPokemon(pokedexId, nivel, mote);
		this.id = idDb;
	}

	private void crearPokemon(int pokedexId, int nivel, String mote) {
		File pokemonData = new File("pokemons.json");
		JsonParser parser = new JsonParser();
		JsonArray pokemonArr;

		try {
			pokemonArr = parser.parse(new FileReader(pokemonData)).getAsJsonArray();

			JsonObject pokemonObj = null;

			for (JsonElement obj : pokemonArr) {
				JsonObject pokObj = obj.getAsJsonObject();

				if (pokObj.get("id").getAsInt() == pokedexId) {
					pokemonObj = pokObj;
				}
			}

			if (pokemonObj != null) {
				this.nivel = nivel;
				this.especie = pokemonObj.get("name").getAsJsonObject().get("english").getAsString();
				this.idPokedex = pokemonObj.get("id").getAsInt();
				if(mote == null) {
					this.mote = this.especie;
				}else {
					this.mote = mote;
				}
				
				JsonArray tiposArr = pokemonObj.get("type").getAsJsonArray();
				for (int i = 0; i < tiposArr.size(); i++) {
					this.tipos.add(tiposArr.get(i).getAsString());
				}

				JsonObject base = pokemonObj.get("base").getAsJsonObject();
				this.psBase = base.get("hp").getAsInt();
				this.ataqueBase = base.get("attack").getAsInt();
				this.defensaBase = base.get("defense").getAsInt();
				this.velocidadBase = base.get("speed").getAsInt();
				this.ataqueEspecialBase = base.get("spAttack").getAsInt();
				this.defensaEspecialBase = base.get("spDefense").getAsInt();

				this.ps = ((2 * this.psBase * this.nivel) / 100) + this.nivel + 10;
				this.psActual = this.ps;
				this.ataque = ((2 * this.ataqueBase * nivel) / 100) + 5;
				this.defensa = ((2 * this.defensaBase * nivel) / 100) + 5;
				this.velocidad = ((2 * this.velocidadBase * nivel) / 100) + 5;
				this.ataqueEspecial = ((2 * this.ataqueEspecialBase * nivel) / 100) + 5;
				this.defensaEspecial = ((2 * this.defensaEspecialBase * nivel) / 100) + 5;
			}
		} catch (JsonIOException | JsonSyntaxException | FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setMote(String newMote) {
		this.mote = newMote;
	}

	@Override
	public String toString() {
		return "\nEspecie: " + this.especie + "\nps: " + this.ps + "\nataque: " + this.ataque + "\ndefensa: "
				+ this.defensa + "\nvelocidad: " + this.velocidad + "\nataqueEspecial: " + this.ataqueEspecial
				+ "\ndefensaEspecial: " + this.defensaEspecial;
	}

	public int getPs() {
		return ps;
	}
	
	public int getPsActual() {
		return psActual;
	}
	
	public void setPsActual() {
		
	}

	public int getAtaque() {
		return ataque;
	}

	public int getDefensa() {
		return defensa;
	}

	public int getNivel() {
		return nivel;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public int getAtaqueEspecial() {
		return ataqueEspecial;
	}

	public int getDefensaEspecial() {
		return defensaEspecial;
	}

	public ArrayList<String> getTipos() {
		return tipos;
	}

	public String getEspecie() {
		return especie;
	}

	public String getMote() {
		return mote;
	}

	public int getId() {
		return id;
	}

	public int getIdPokedex() {
		return idPokedex;
	}
	
	public void remove() {
		System.out.println("Borrado en la mochila -> " + this.id);
		this.idPokedex = -2;
	}

}
