package dam_practicafinal.entidades;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TiendaStock {
	public JsonArray getAllObjetos() {
		File objetoData = new File("objeto.json");
		JsonParser parser = new JsonParser();
		JsonArray objetoArr = null;

		try {
			objetoArr = parser.parse(new FileReader(objetoData)).getAsJsonArray();
		}catch(IOException ex){
			
		}
		
		return objetoArr;
	}
}
