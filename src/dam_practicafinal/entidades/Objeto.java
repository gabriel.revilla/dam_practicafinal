package dam_practicafinal.entidades;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Objeto {
	private int cantidad;
	private int objetoId;
	private int id;
	private String tipo;
	private String nombre;

	public Objeto(int objetoId, int cantidad) {
		this.objetoId = objetoId;
		this.cantidad = cantidad;
		this.id = -1;
		this.crearObjeto(objetoId);
	}

	public Objeto(int objetoId, int cantidad, int id) {
		this.objetoId = objetoId;
		this.cantidad = cantidad;
		this.id = id;
		this.crearObjeto(objetoId);
	}
	
	private void crearObjeto(int objetoId) {
		File objetoData = new File("objeto.json");
		JsonParser parser = new JsonParser();
		JsonArray objetoArr;

		try {
			objetoArr = parser.parse(new FileReader(objetoData)).getAsJsonArray();
			
			JsonObject objetoObj = null;
			
			for(int i=0;i<objetoArr.size();i++) {
				JsonObject objetoObjAux = objetoArr.get(i).getAsJsonObject();
				
				if(objetoObjAux.get("id").getAsInt() == objetoId) {
					objetoObj = objetoArr.get(i).getAsJsonObject();
				}
			}
			
			if(objetoObj != null) {
				
				this.tipo = objetoObj.get("tipo").getAsString();
				this.nombre = objetoObj.get("nombre").getAsString();
			}
		}catch(IOException ex){
			
		}
	}

	public boolean add(int cantidad) {
		this.cantidad += cantidad;
		return true;
	}

	public boolean usarObjeto(int cant) {
		if (this.cantidad <= 0) {
			return false;
		} else {
			this.cantidad -= cant;
			return true;
		}
	}
	
	public int getCantidad() {
		return this.cantidad;
	}

	public int getObjetoId() {
		return this.objetoId;
	}
	
	public int getId() {
		return this.id;
	}

	public String getTipo() {
		return this.tipo;
	}
	
	public String getNombre() {
		return this.nombre;
	}
}
