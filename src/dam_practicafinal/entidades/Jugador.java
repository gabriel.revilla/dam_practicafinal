/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dam_practicafinal.entidades;

import dam_practicafinal.tools.DbHwnd;
import java.io.File;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author adrian
 */
public class Jugador {
	private static final int INITIAL_POKECUARTOS = 500;
	private static final int INITIAL_OBJECT = 5;
	private static final int INITIAL_AMOUNT_OBJECT = 10;

	private int id;
	private String user;
	private Date fechaNacimiento;
	private String sexo;
	private DbHwnd dbHwnd;
	private Mochila mochila;

	public Jugador() {
		dbHwnd = new DbHwnd(new File("config.json"));
	}

	public boolean login(String user, String password) {
		try {
			PreparedStatement ps = dbHwnd.getConnection()
					.prepareStatement("SELECT * FROM jugadores WHERE user=? and password=?");
			ps.setString(1, user);
			ps.setString(2, password);
			ResultSet result = dbHwnd.query(ps);

			if (result.next()) {
				this.fechaNacimiento = result.getDate("fecha_nacimiento");
				this.sexo = result.getString("sexo");
				this.id = result.getInt("id");
				this.user = user;
				this.mochila = new Mochila(this.id);

				return true;
			} else {
				return false;

			}

		} catch (SQLException ex) {
			Logger.getLogger(Jugador.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

	}

	public boolean register(String user, String password, String sexo, Date fechaNacimiento) {
		try {
			PreparedStatement ps = dbHwnd.getConnection().prepareStatement("SELECT * FROM jugadores WHERE user=?");
			ps.setString(1, user);
			ResultSet result = dbHwnd.query(ps);

			if (result.next()) {
				return false;
			} else {

				dbHwnd.getConnection().setAutoCommit(false);

				/////////////////// Creacion del Jugador//////////////////////
				PreparedStatement userStmt = dbHwnd.getConnection().prepareStatement(
						"INSERT INTO jugadores (user, password, fecha_nacimiento, sexo)" + "VALUES (?,?,?,?)");
				userStmt.setString(1, user);
				userStmt.setString(2, password);
				userStmt.setDate(3, fechaNacimiento);
				userStmt.setString(4, String.valueOf(sexo.charAt(0)));

				dbHwnd.insert(userStmt);

				/////////////////// Creacion de la mochila//////////////////////
				PreparedStatement ups = dbHwnd.getConnection().prepareStatement("SELECT * FROM jugadores WHERE user=?");
				ups.setString(1, user);
				result = dbHwnd.query(ups);
				result.next();
				int idNewJugador = result.getInt("id");

				dbHwnd.getConnection().setAutoCommit(false);
				PreparedStatement mochilaStmt = dbHwnd.getConnection()
						.prepareStatement("INSERT INTO mochilas (id_jugador,pokecuartos)" + "VALUES (?,?)");
				mochilaStmt.setInt(1, idNewJugador);
				mochilaStmt.setInt(2, INITIAL_POKECUARTOS);

				dbHwnd.insert(mochilaStmt);

				/////////////////// Creacion del objeto//////////////////////
				PreparedStatement mps = dbHwnd.getConnection()
						.prepareStatement("SELECT * FROM mochilas WHERE id_jugador=?");
				mps.setInt(1, idNewJugador);
				result = dbHwnd.query(mps);
				result.next();
				int idNewMochila = result.getInt("id");

				dbHwnd.getConnection().setAutoCommit(false);
				PreparedStatement objetoStmt = dbHwnd.getConnection()
						.prepareStatement("INSERT INTO objetos (id_mochila, id_objeto, cantidad)" + "VALUES (?,?,?)");
				objetoStmt.setInt(1, idNewMochila);
				objetoStmt.setInt(2, INITIAL_OBJECT);
				objetoStmt.setInt(3, INITIAL_AMOUNT_OBJECT);

				dbHwnd.insert(objetoStmt);

				/////////////////////////////////////////////////////////////

				dbHwnd.getConnection().commit();

				dbHwnd.getConnection().setAutoCommit(true);

				return true;
			}

		} catch (SQLException ex) {
			Logger.getLogger(Jugador.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}

	}

	public String getUser() {
		return user;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public String getSexo() {
		return sexo;
	}

	public int getId() {
		return this.id;
	}
	
	public Mochila getMochila() {
		return this.mochila;
	}
}
