package dam_practicafinal.entidades;

import dam_practicafinal.tools.DbHwnd;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Mochila {
	private final int MAX_POKEMONS = 200;
	private final int MAX_OBJETOS = 100;

	private int idMochila;
	private int idJugador;
	private int pokecuartos;
	private ArrayList<Objeto> objetos;
	private ArrayList<Pokemon> pokemones;
	private DbHwnd dbHwnd;

	public Mochila(int idJugador) {
		this.idJugador = idJugador;
		this.dbHwnd = new DbHwnd(new File("config.json"));
		this.pokemones = new ArrayList<Pokemon>();
		this.objetos = new ArrayList<Objeto>();
		
		try {
			PreparedStatement ps = dbHwnd.getConnection().prepareStatement("SELECT * FROM mochilas WHERE id_jugador=?");
			ps.setInt(1, idJugador);
			ResultSet result = dbHwnd.query(ps);

			if(result.next()) {
				this.idMochila = result.getInt("id");
				this.pokecuartos = result.getInt("pokecuartos");
				updatePokemons(this.idMochila);
				updateObjetos(this.idMochila);
			}
		} catch (SQLException ex) {
			Logger.getLogger(Mochila.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void updatePokemons(int idMochila) {
		try {
			dbHwnd = new DbHwnd(new File("config.json"));
			PreparedStatement ps = dbHwnd.getConnection().prepareStatement("SELECT * FROM pokemones WHERE id_mochila=?");
			ps.setInt(1, idMochila);
			ResultSet result = dbHwnd.query(ps);
			while (result.next()) {
				pokemones.add(new Pokemon(result.getInt("id_pokedex"), result.getInt("nivel"), result.getString("mote"),
						result.getInt("id")));
			}

		} catch (SQLException ex) {
			Logger.getLogger(Mochila.class.getName()).log(Level.SEVERE, null, ex);
		}

	}

	private void updateObjetos(int id) {
		try {
			dbHwnd = new DbHwnd(new File("config.json"));
			PreparedStatement ps = dbHwnd.getConnection()
					.prepareStatement("SELECT * FROM objetos WHERE id_mochila=?");
			ps.setInt(1, idMochila);
			ResultSet result = dbHwnd.query(ps);
			while (result.next()) {
				objetos.add(new Objeto(result.getInt("id_objeto"), result.getInt("cantidad"), result.getInt("id")));
			}

		} catch (SQLException ex) {
			Logger.getLogger(Mochila.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public boolean addPokemon(Pokemon newPokemon) {
		if (pokemones.size() <= MAX_POKEMONS) {
			pokemones.add(newPokemon);
			return true;
		} else {
			return false;
		}
	}
	
	public void removePokemon(int id) {
		for(int i=0;i<this.pokemones.size();i++) {
			if(this.pokemones.get(i).getId() == (id+1)) {
				pokemones.get(this.pokemones.get(i).getId()).remove();
			}
		}
	}

	public boolean addObjetos(Objeto newObjeto) {
		int cantidadObjetos = 0;
		for (int i = 0; i < objetos.size(); ++i) {
			cantidadObjetos += objetos.get(i).getCantidad();
		}

		cantidadObjetos += newObjeto.getCantidad();

		if (cantidadObjetos <= MAX_OBJETOS || MAX_OBJETOS == -1) {
			boolean flag = false;

			for (int i = 0; i < objetos.size(); ++i) {
				Objeto objeto = objetos.get(i);
				if (objeto.getObjetoId() == newObjeto.getObjetoId()) {
					objetos.get(i).add(newObjeto.getCantidad());

					flag = true;
				}
			}

			if (!flag)
				objetos.add(newObjeto);
			return true;

		} else {
			return false;
		}
	}

	public boolean guardarDb() {
		try {
			dbHwnd.getConnection().setAutoCommit(false);

			PreparedStatement prepStmt = dbHwnd.getConnection()
					.prepareStatement("UPDATE mochilas SET pokecuartos=? WHERE id=?");
			prepStmt.setInt(1, this.pokecuartos);
			prepStmt.setInt(2, this.idMochila);

			dbHwnd.insert(prepStmt);

			for (int i = 0; i < this.pokemones.size(); i++) {
				Pokemon savePokemon = this.pokemones.get(i);

				if(savePokemon.getIdPokedex() == -1) {
					System.out.println("BORRANDO -> " + savePokemon.getId());
					
					PreparedStatement pokeStmt = dbHwnd.getConnection()
							.prepareStatement("DELETE FROM pokemones WHERE id=?");
					pokeStmt.setInt(1, savePokemon.getId());

					dbHwnd.insert(pokeStmt);
				}else {
					if (savePokemon.getId() != -1) {
						PreparedStatement pokeStmt = dbHwnd.getConnection()
								.prepareStatement("UPDATE pokemones SET nivel=?, mote=? WHERE id=?");
						pokeStmt.setInt(1, savePokemon.getNivel());
						pokeStmt.setString(2, savePokemon.getMote());
						pokeStmt.setInt(3, savePokemon.getId());

						dbHwnd.insert(pokeStmt);
					} else {
						PreparedStatement pokeStmt = dbHwnd.getConnection().prepareStatement(
								"INSERT INTO pokemones (id_mochila, id_pokedex, mote, nivel) VALUES (?,?,?,?)");
						pokeStmt.setInt(1, this.idMochila);
						pokeStmt.setInt(2, savePokemon.getIdPokedex());
						pokeStmt.setString(3, savePokemon.getMote());
						pokeStmt.setInt(4, savePokemon.getNivel());

						dbHwnd.insert(pokeStmt);
					}
				}
				
			}
			
			for(int i=0;i<this.objetos.size();i++) {
				Objeto saveObjeto = this.objetos.get(i);
				
				if (saveObjeto.getId() != -1) {
					PreparedStatement objStmt = dbHwnd.getConnection()
							.prepareStatement("UPDATE objetos SET cantidad=? WHERE id=?");
					objStmt.setInt(1, saveObjeto.getCantidad());
					objStmt.setInt(2, saveObjeto.getId());

					dbHwnd.insert(objStmt);
				} else {
					PreparedStatement objStmt = dbHwnd.getConnection().prepareStatement(
							"INSERT INTO objetos (id_mochila, id_objeto, cantidad) VALUES (?,?,?)");
					objStmt.setInt(1, this.idMochila);
					objStmt.setInt(2, saveObjeto.getObjetoId());
					objStmt.setInt(3, saveObjeto.getCantidad());

					dbHwnd.insert(objStmt);
				}
			}

			dbHwnd.getConnection().commit();
			dbHwnd.getConnection().setAutoCommit(true);
			
			return true;
		} catch (SQLException ex) {
			Logger.getLogger(Mochila.class.getName()).log(Level.SEVERE, null, ex);
			return false;
		}
	}
	
	public ArrayList<Pokemon> getPokemones(){
		return this.pokemones;
	}
	
	public ArrayList<Objeto> getObjetos(){
		return this.objetos;
	}
	
	public int getPokecuartos() {
		return this.pokecuartos;
	}
	
	public boolean descontarPkc(int cantidad) {
		if(this.pokecuartos >= cantidad) {
			this.pokecuartos -= cantidad;
			return true;
		}else {
			return false;
		}
	}
	
	public void addPkc(int cantidad) {
		this.pokecuartos += cantidad;
	}
}
