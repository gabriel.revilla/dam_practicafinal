package dam_practicafinal.entidades;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class Pokedex {
	private File pokemonData;
	
	public Pokedex() {
		pokemonData = new File("pokemons.json");
	}
	
	public JsonArray getAllPokedex() {
		JsonParser parser = new JsonParser();
		JsonArray pokemonArr = null;

		try {
			pokemonArr = parser.parse(new FileReader(pokemonData)).getAsJsonArray();
		}catch(IOException ex) {
			ex.printStackTrace();
		}
		
		return pokemonArr;
	}
}
